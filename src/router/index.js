import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '首页',
      redirect: '/signup'
    },
    {
      path: '/signup',
      name: 'signUp',
      component: resolve => {
        require(['@/components/signUp.vue'], resolve)
      }
    },
    {
      path: '/success/:haveTicket',
      name: 'success',
      component: resolve => {
        require(['@/components/success.vue'], resolve)
      }
    },
    {
      path: '/fillinfo',
      name: 'fillinfo',
      component: resolve => {
        require(['@/components/fillInfo.vue'], resolve)
      }
    }
  ]
})
