import  axios from  'axios'
import  qs from  'qs'

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
//axios.defaults.withCredentials = true;  // 跨域请求
//axios.defaults.headers.post['Content-Type'] = 'multipart/from-data'
//axios.defaults.headers.post['Content-Type'] = 'application/json'
export function addInfo(param) {
    const url = 'https://changiwechat.azurewebsites.net/WeChatAPI/Activity/RetainAdventuresStar'  
    return axios.post(url, qs.stringify(param))
}